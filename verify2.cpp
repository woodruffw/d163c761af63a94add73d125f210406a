STACK_OF(X509) *certs = p7->d.sign->cert;

/* Re-serialize the SpcIndirectDataContent to DER... */
std::uint8_t indir_data_buf = nullptr;
i2d_Authenticode_SpcIndirectDataContent(indir_data, &indir_data_buf);

/* ...so that we can unwrap its sequence here. */
const auto *signed_data_seq = indir_data_buf;
long length = 0;
int tag = 0, tag_class = 0;
ASN1_get_object(&signed_data_seq, &length, &tag, &tag_class, buf_size);
assert(tag == V_ASN1_SEQUENCE);

auto *signed_data = BIO_new_mem_buf(signed_data_seq, length);

/* Our stack of certs isn't guaranteed to include the root cert,
 * so pass PKCS7_NOVERIFY.
 */
PKCS7_verify(p7, certs, nullptr, signed_data, nullptr, PKCS7_NOVERIFY);

assert(status == 1);