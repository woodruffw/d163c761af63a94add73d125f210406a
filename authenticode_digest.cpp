auto *md = indir_data->messageDigest;
auto nid = OBJ_obj2nid(md->digestAlgorithm->algorithm);
auto digest = std::vector<std::uint8_t>(md->digest->data, md->digest->data + md->digest->length);