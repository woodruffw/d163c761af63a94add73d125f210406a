struct impl_page_hash {
    /* page_offset corresponds to a real file offset within each section,
     * and should be aligned by page size.
     */
    uint32_t page_offset;
    uint8_t  page_hash[IMPL_PAGE_HASH_SIZE];
} __attribute__((packed));

typedef impl_page_hash *impl_page_hashes;