#include <uthenticode.h>

#include <iostream>

int main(int argc, char **argv) {
    auto *pe = peparse::ParsePEFromFile(argv[1]);

    std::cout << argv[1] << " has a "
              << (uthenticode::verify(pe) ? "valid" : "invalid")
              << " signature!";
}