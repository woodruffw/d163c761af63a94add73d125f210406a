struct win_certificate {
    uint32_t length;                    /* dwLength */
    uint16_t revision;                  /* wRevision */
    uint16_t certificate_type;          /* wCertificateType */
    uint8_t  certificate[/* length */]; /* bCertificate */
} __attribute__((aligned (8)));