/* Assuming that buf is an OpenSSL BIO* containing the DER-encoded PKCS#7 object */
auto *p7 = d2i_PKCS7_bio(buf, nullptr);
auto *contents = p7->d.sign->contents;

/* The d2i_ family increments the pointer passed to it, so we make a copy. */
auto *indir_data_inc_ptr = contents->d.other->value.sequence->data;
auto *indir_data = d2i_Authenticode_SpcIndirectDataContent(
    nullptr, &indir_data_inc_ptr, contents->d.other->value.sequence->length);